# Test task for the Laravel developer position
Create a web parsing basis functionality with the next logic:

1. Load data about articles from https://laravel-news.com/blog with tag 'news' for the last 4 months
2. Show loaded data on the main page as a table with the next fields:
    * publication date in a format - 'd.m.Y'
    * title as a link to the article page
    * author name
    * all tags associated with article separated by a comma
3. Articles should be sorted alphabetically by author name
4. A possibility of manual sorting by title and date should be available also
5. Articles should be loaded once and saved in DB
6. Every page reload should show data from DB
7. Additionally add a command for updating the articles' data
# Test task for the Laravel developer position
Create a web parsing basis functionality with the next logic:

1. Load data about articles from [https://laravel-news.com/blog](https://laravel-news.com/blog) with tag 'news' for the last 4 months
2. Show loaded data on the main page as a table with the next fields:
    * publication date in a format - 'd.m.Y'
    * title as a link to the article page
    * author name
    * all tags associated with article separated by a comma
3. Articles should be sorted alphabetically by author name
4. A possibility of manual sorting by title and date should be available also
5. Articles should be loaded once and saved in DB
6. Every page reload should show data from DB
7. Additionally add a command for updating the articles' data
8. Not required but will be a plus if the project be built using docker containers

#### Requirements
Functionality should be built by using Laravel

OOP and SOLID principles should be used

Code should be well commented

#### You should use at least next:
* Laravel 11.x
* MySQL
* composer
* git

#### User interface
There are no strict requirements for the interface. However, a pretty interface may be a plus

#### The result
The test task result should be a Laravel based project with at lead described functionality.
Any additional features or code structures demonstration may be a plus

All needed instructions for run the project should be described in 'readme.md'

The project should be stored in a git repository located on [bitbucket.org](https://bitbucket.org), or a similar service